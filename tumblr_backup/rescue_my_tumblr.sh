#!/bin/sh

MY_BLOG="example.tumblr.com"
tumblr_backup=/usr/local/bin/tumblr_backup.py

mkdir -p ~/backup/tumblr2018/
cd ~/backup/tumblr2018/

python $tumblr_backup $MY_BLOG
python $tumblr_backup --csv=true $MY_BLOG
cd $MY_BLOG
for h in `ls *.html`; do 
	html2xpath.py $h img src|while read i;do cd images; wget -nc $i; done
done; cd ..
