#!/bin/sh

MY_BIN="/usr/local/bin/"
sudo mkdir -p $MY_BIN
if [ -d "$MY_BIN" ]; then
	sudo chmod 0755 html2xpath.py tumblr_evacuate tumblr_backup/tumblr_backup.py
	sudo cp -i html2xpath.py tumblr_evacuate tumblr_backup/tumblr_backup.py $MY_BIN
	echo $(tumblr_evacuate)
else
	echo "[err] unable to install (check $MY_BIN exists and that you have sudo)"
fi
