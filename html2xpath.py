#!/usr/bin/env python3
#html2xpath.py ver. 20181128135355 Copyright 2018 isobel, MIT Licence
#rdfa:deps="['sudo pip3 install ConfigParser']"
import os,sys,re,requests

if len(sys.argv) < 2 or not os.path.isfile(sys.argv[1]):
   print("Usage: html2xapth.py </tmp/example.html> img src")
   sys.exit(1)

with requests.Session() as session:
   from bs4 import BeautifulSoup
   soup = BeautifulSoup(open(sys.argv[1]), 'html.parser')
   if len(sys.argv) == 3:
      for match in soup.find_all(sys.argv[2]):
         print(match)
   elif len(sys.argv) == 4:
      for match in soup.find_all(sys.argv[2]):
         print(match[sys.argv[3]])

"""__END__"""
